import win32com.client, csv, datetime, smtplib, time, shutil, os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from selenium import webdriver


try:
    start_time = datetime.datetime.now()
    print(start_time)

    #provide xml template name
    prefix = 'nsx'




    #TODO horizon login


    print('start browser')
    browserBI = webdriver.Chrome()
    browserBI.implicitly_wait(30)

    print('login to horizon')
    #no horizon login module - but it works for now :)
    browserBI.get('https://entitlementbi-prod-sso.vmware.com/analytics/saw.dll?Answers&SubjectArea=%22Quotable%20Assets%22')
    browserBI.find_element_by_xpath('//td[@title="Edit SQL, XML and other technical details"]').click()

    #upload xml template from file
    print('upload file template')
    f = open('H:\\Endurance_reports\\bi_xml\\{}.txt'.format(prefix))
    template_lines = f.readlines()
    f.close()

    browserBI.find_element_by_name('XmlText').clear()
    browserBI.find_element_by_name('XmlText').send_keys(template_lines)
    browserBI.find_element_by_id('advancedTabApplyXmlButton').click()
    print('template ready')
    for i in range(20):
        time.sleep(1)
        
    browserBI.find_element_by_xpath('//td[@title="Display and edit views of results"]').click()

    print('waiting for report')
    #wating module
    for m in range(6):
        try:
            browserBI.find_element_by_xpath('//td[text()="Table"][@class="CVUIViewHdg"]').get_attribute('innerHTML')
        except:
            print('waiting {}min'.format(m+1))
            for s in range(30):
                time.sleep(2)
            pass

    print('exporting')
    #exporting
    browserBI.find_element_by_xpath('//a[@title="Export this analysis"]').click()
    browserBI.find_element_by_xpath('//td[text()="Data"]').click()
    browserBI.find_element_by_xpath('//td[text()="CSV Format"]').click()


    #format excel file
    print('start excel formating')


    #check if download is still in progress
    print('check if download is still in progress')
    for i in range(20):
        time.sleep(1)
        
    while True:
        
        size1 = os.path.getsize("D:\\Users\\pnawrocki\\Downloads\\Untitled Analysis.csv")
        for i in range(30):
            time.sleep(1)
        size2 = os.path.getsize("D:\\Users\\pnawrocki\\Downloads\\Untitled Analysis.csv")
        if size1 == size2:
            break
        else:
            print('download still in progress + 30sec')

    #move csv and change name
    print('move and rename file')
    dt = datetime.datetime.now()
    inputfile = "H:\\Endurance_reports\\{0}\\{0}_csv_{1}.csv".format(prefix,dt.strftime('%Y%m%d%H%M'))
    os.makedirs("H:\\Endurance_reports\\{}".format(prefix), exist_ok=True)
    shutil.move("D:\\Users\\pnawrocki\\Downloads\\Untitled Analysis.csv", inputfile)
    browserBI.quit()

    #open excel
    print('open the excel')
    excel = win32com.client.gencache.EnsureDispatch('Excel.Application')
    #excel.Visible = True
    
    inwb = excel.Workbooks.Open(inputfile)
    inws = inwb.Sheets(1)

    otwb = excel.Workbooks.Open('H:\\Endurance_reports\\bi_xml\\nsx.xlsx')
    otws = otwb.Sheets(1)

    #how many rows
    print('check how many rows')
    row_num = 2
    while inws.Cells(row_num,1).Value != None:
            row_num += 1
    print(row_num)

    #copy data
    print('copy data')
    inws.Range("A1:O{}".format(row_num)).Copy(otws.Range("A1:O{}".format(row_num)))

    #save
    print('save excel')
    outputfile = '{0}_xlsx_{1}.xlsx'.format(prefix,dt.strftime('%Y%m%d%H%M'))
    outputpath = 'H:\\Endurance_reports\\{0}\\{0}_xlsx_{1}.xlsx'.format(prefix,dt.strftime('%Y%m%d%H%M'))
    otwb.SaveAs(outputpath)

    inwb.Close(False)
    otwb.Close(False)
    excel.Quit()

    #TODO mail distribution
    print('send email')
    fromaddr = "pnawrocki@XXXXXXX"
    toaddr = "pnawrocki@XXXXXXXX"
     
    msg = MIMEMultipart()
     
    msg['From'] = fromaddr
    msg['To'] = toaddr
    msg['Subject'] = "Edurance report - {}".format(prefix)
     
    body = "REPORT ATTACHED"
     
    msg.attach(MIMEText(body, 'plain'))
     
    
    attachment = open(outputpath, "rb")
     
    part = MIMEBase('application', 'octet-stream')
    part.set_payload((attachment).read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', "attachment; filename= {}".format(outputfile))
     
    msg.attach(part)
     
    server = smtplib.SMTP('mail.office365.com', 587)
    server.starttls()
    server.login(fromaddr, "XXXXXXX")
    text = msg.as_string()
    server.sendmail(fromaddr, toaddr, text)
    server.quit()

    print('email sent')

    end_time = datetime.datetime.now()
    print(end_time)
    delta_time = end_time - start_time
    print("program live time {}".format(delta_time))
    print('END')
except:
    print('HELP -- ERROR ERROR ERROR')
    input()